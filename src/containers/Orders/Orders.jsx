import React, { Component } from 'react';

import axios from '../../axiosInstance';

import Order from '../../components/Order/Order';
import Spinner from '../../components/UI/Spinner/Spinner';

class Orders extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orders: null
        }
    }

    componentDidMount() {
        axios.get('orders.json').then(response => {
            const fetchedOrders = [];
            for (let key in response.data) {
                fetchedOrders.push({
                    ...response.data[key],
                    id: key
                })
            }
            this.setState({ orders: fetchedOrders });
        }).catch(() => {
            this.setState({ error: true });
        });
    }

    render() {

        let orderList = <Spinner />
        if (this.state.orders) {
            orderList = this.state.orders.map(order => <Order ingredients={order.order} price={order.orderPrice} />)
        }

        return (
            <div>
                {orderList}
            </div>
        );
    }
}

export default Orders;