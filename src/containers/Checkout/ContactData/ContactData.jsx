import React, { Component } from 'react';
import classes from './ContactData.css';
import axios from '../../../axiosInstance';
import update from 'immutability-helper';
import {connect} from 'react-redux';

import Input from '../../../components/UI/Input/Input';
import Spinner from '../../../components/UI/Spinner/Spinner';
import Button from '../../../components/UI/Button/Button';

class ContactData extends Component {
    constructor(props) {
        super(props);
        this.state = {
            orderForm: {
                name: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Twoje imię',
                        value: ''
                    },
                    validation: {
                        required: true
                    },
                    valid: false,
                    touched: false
                },
                surname: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Twoje nazwisko',
                        value: ''
                    },
                    validation: {
                        required: true
                    },
                    valid: false,
                    touched: false
                },
                address: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'adress',
                        value: ''
                    },
                    validation: {
                        required: true
                    },
                    valid: false,
                    touched: false
                },
                street: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'ulica',
                        value: ''
                    },
                    validation: {
                        required: true
                    },
                    valid: false,
                    touched: false
                },
                zipCode: {
                    elementType: 'input',
                    elementConfig: {
                        type: 'text',
                        placeholder: 'Kod pocztowy',
                        value: ''
                    },
                    validation: {
                        required: true,
                        minLength: 5,
                        maxLength: 5
                    },
                    valid: false,
                    touched: false
                },
                deliveryMetod: {
                    elementType: 'select',
                    elementConfig: {
                        options: [
                            { value: 'fast', display: 'szybko' },
                            { value: 'cheap', display: 'tanio' }
                        ],
                        value: 'cheap'
                    },
                    validation: {},
                    valid: true,
                    touched: false
                }
            },
            formIsValid: false,
            loading: false,
        }
    }

    handleSendOrder = () => {
        this.setState({ loading: true });
        let customer = {}
        for(let formField in this.state.orderForm){
            // console.dir(formField)
            if(formField !== 'deliveryMetod'){
                customer[formField] = this.state.orderForm[formField].elementConfig.value;
            }
        }
        const orderData = {
            customer: customer,
            order: this.props.ings,
            orderPrice: this.props.price.toFixed(2),
            delivery: this.state.orderForm.deliveryMetod.elementConfig.value
        }
        axios.post('orders.json', orderData).then(response => {
            this.setState({ loading: false, purchasing: false });
            this.props.history.replace('/');
        }).catch(response => {
            this.setState({ loading: false, purchasing: false });
        });
    }

    checkValidation(value, validationConfig) {
        let isValid = true;

        if (validationConfig.required) {
            isValid = value.trim() !== '' && isValid;
        }

        if (validationConfig.minLength) {
            isValid = value.length >= validationConfig.minLength && isValid;
        }

        if (validationConfig.maxLength) {
            isValid = value.length <= validationConfig.maxLength && isValid;
        }

        return isValid;
    }

    onChangeHandler = (event, inputElement) => {
        let inputIsValid = this.checkValidation(event.target.value, this.state.orderForm[inputElement].validation)
        let updatedForm = update(this.state.orderForm, {
            [inputElement]: {
                elementConfig: { value: { $set: event.target.value } },
                valid: { $set: inputIsValid },
                touched: {$set: true}
            }
        });
        let formIsValid = true;
        for (let element in updatedForm) {
            formIsValid = updatedForm[element].valid && formIsValid;
        }
        this.setState({ orderForm: updatedForm, formIsValid: formIsValid });

    }

    render() {
        let fetchedInputs = [];
        for (let key in this.state.orderForm) {
            fetchedInputs.push(
                {
                    id: key,
                    config: this.state.orderForm[key]
                }
            );
        }

        let form = (
            <form onSubmit={this.handleSendOrder}>
                {fetchedInputs.map(input => (
                    <Input key={input.id}
                        changed={(event) => this.onChangeHandler(event, input.id)}
                        elementType={input.config.elementType}
                        config={input.config.elementConfig}
                        valid={input.config.valid}
                        touched={input.config.touched}
                    />
                ))}
                <Button
                    type="Positive"
                    textValue="Zamów"
                    disabled={!this.state.formIsValid}
                />
            </form>
        )
        if (this.state.loading) {
            form = <Spinner />
        }
        return (
            <div className={classes.ContactData}>
                <p>Wprowadź swoje dane dostawy</p>
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        ings: state.ingredients,
        price: state.totalPrice
    }
}


export default connect(mapStateToProps)(ContactData);