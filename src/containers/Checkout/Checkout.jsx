import React, { Component } from 'react';
import { Route } from 'react-router';
import {connect} from 'react-redux';

import CheckoutSummary from '../../components/Order/CheckoutSummary/CheckoutSummary'
import ContactData from './ContactData/ContactData';
class Checkout extends Component {
    constructor(props) {
        super(props);
        this.state = {
            ingredients: null,
            totalPrice: null
        }
    }
    checkoutContinueHandler = () => {
        this.props.history.push({
            pathname: this.props.match.path + "/contactdata"
        })
    }
    checkoutCancelHandler = () => {
        this.props.history.push({
            pathname: "/"
        })
    }
    componentWillMount() {
        if(this.props.price === 4) this.props.history.push('/');
    }

    render() {
        return (
            <div>
                <CheckoutSummary
                    ingredients={this.props.ings}
                    checkoutContinued={this.checkoutContinueHandler}
                    checkoutCanceled={this.checkoutCancelHandler}
                />
                <Route
                    path={this.props.match.path + "/contactdata"}
                    component={ContactData}
                />
            </div>
        )
    }
}

const mapStateToProps = state => {
    return {
        ings: state.ingredients,
        price: state.totalPrice
    }
}

export default connect(mapStateToProps)(Checkout);