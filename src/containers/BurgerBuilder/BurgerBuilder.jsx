import React, { Component } from 'react';
import {connect} from 'react-redux';
import { bindActionCreators } from 'redux'

import Burger from '../../components/Burger/Burger';
import BurgerControls from '../../components/Burger/BurgerControls/BurgerControls';
import Modal from '../../components/UI/Modal/Modal';
import OrderSummary from '../../components/Burger/OrderSummary/OrderSummary';
import Spinner from '../../components/UI/Spinner/Spinner';
import withErrorHandler from '../../hoc/withErrorHandler/withErrorHandler';
import axios from '../../axiosInstance';
import Auxi from '../../hoc/Auxs/Auxi';
import * as actionCreators from '../../store/actions/index';

class BurgerBuilder extends Component {
    constructor(props) {
        super(props);
        this.state = {
            purchasing: false,
            loading: false,
            error: false,
        }
    }
 
    componentDidMount() {
        axios.get('ingredients').then(response => {
            this.setState({ ingredients: response.data });
        }).catch((response) => {
            this.setState({ error: true });
            console.log(response);
        });
    }

    updatePurchasingState = () => {
        this.setState((prevState) => ({
            purchasing: !prevState.purchasing
        }));
    }
    updatePurchaseableState = (updatedIngretients) => {
        let ingredients = updatedIngretients;

        const sum = Object
            .keys(ingredients)
            .map(igKey => ingredients[igKey])
            .reduce((sum, el) => {
                return sum = sum + el;
            });
         return sum > 0
    }
 
    handleContinueToOrder = () => {
        this.props.history.push({
            pathname: '/checkout'
        });
    }

    render() {
        let orderSummary = < OrderSummary
            totalPrice={this.props.price}
            ingredients={this.props.ings}
            clickedHandlers={
                {
                    discardClick: this.updatePurchasingState,
                    continueClick: this.handleContinueToOrder
                }
            }
        />

        let burger = this.state.error ? <p> Błąd podczas ładowania składników </p> : <Spinner />
        if (this.state.loading || !this.props.ings) {
            orderSummary = < Spinner />
        }
        if (this.props.ings) {
            let disabledInfo = {
                ...this.props.ings
            }
            for (var key in disabledInfo) {
                disabledInfo[key] = disabledInfo[key] <= 0;
            }

            burger =
                (<Auxi>
                    <Burger
                        ingredients={this.props.ings}
                    /> <BurgerControls removedIngredient={this.props.actions.removeIngredient}
                        addedIngredient={this.props.actions.addIngredient}
                        disabled={disabledInfo}
                        totalPrice={this.props.price}
                        purchaseable={this.updatePurchaseableState(this.props.ings)}
                        clicked={this.updatePurchasingState}
                    />
                </Auxi>);
        }

        return <Auxi>
            <Modal show={this.state.purchasing}
                modalClicked={this.updatePurchasingState}> 
                {orderSummary}
            </Modal>
            {burger} </Auxi>
    }
}

const mapStateToProps = state => {
    return {
        ings: state.ingredients,
        price: state.totalPrice
    }
}

const mapDispatchToProps = dispatch => {
    return {
        actions: bindActionCreators(actionCreators, dispatch)
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withErrorHandler(BurgerBuilder, axios));