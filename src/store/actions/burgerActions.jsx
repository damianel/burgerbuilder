import * as actionTypes from './actionTypes';

export const initIngredients = () => {
    return dispatch => {

    }
}

export const initIngredientsSuccess = (ing) => {
    return {
        type: actionTypes.INIT_INGREDIENTS_SUCCESS,
        ingredients: ing
    }
}

export const initIngredientsFailed = () => {
    
}

export const addIngredient = (type) => {
    return {
        type: actionTypes.ADD_INGREDIENT,
        ingName: type
    }
}

export const removeIngredient = (type) => {
    return {
        type: actionTypes.REMOVE_INGREDIENT,
        ingName: type
    }
}