import * as actionTypes from './actions/actionTypes';

const initialState = {
    ingredients: {
        meat: 0,
        cheese: 0,
        salad: 0,
        bacon: 0
    },
    totalPrice: 4,
}

const INGREDIENTS_PRICES = {
    meat: 1.2,
    cheese: 0.8,
    salad: 0.4,
    bacon: 1.9
}

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ADD_INGREDIENT:
            return {
                ...state,
                ingredients: {
                    ...state.ingredients,
                    [action.ingName]: state.ingredients[action.ingName] + 1
                },
                totalPrice: state.totalPrice + INGREDIENTS_PRICES[action.ingName]
            };
        case actionTypes.REMOVE_INGREDIENT:
            return {
                ...state,
                ingredients: {
                    ...state.ingredients,
                    [action.ingName]: state.ingredients[action.ingName] - 1
                },
                totalPrice: state.totalPrice - INGREDIENTS_PRICES[action.ingName]
            };
        default: return state;
    }
}

export default reducer;