import React, {Component} from 'react';
import classes from './Layout.css';

import Toolbar from '../../components/UI/Toolbar/Toolbar';
import SideDrawer from '../../components/UI/SideDrawer/SideDrawer';
import Auxi from '../../hoc/Auxs/Auxi';

class Layout extends Component {
    constructor(props){
        super(props);
        this.state = {
            showSideDrawer: false
        }
    }
    handleToggleSidedrawer = () => {
        this.setState((prevstate) => ({showSideDrawer: !prevstate.showSideDrawer}));
    }
    render() {
        return (
            <Auxi>
                <Toolbar toggleSidedrawer={this.handleToggleSidedrawer}/>
                <SideDrawer show={this.state.showSideDrawer} backDropClick={this.handleToggleSidedrawer}/>
                <main className={classes.Content}>
                    {this.props.children}
                </main>
            </Auxi>
        )
    }
}

export default Layout;