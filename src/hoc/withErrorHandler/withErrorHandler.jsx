import React, {Component} from 'react';

import Modal from '../../components/UI/Modal/Modal';
import Auxi from '../Auxs/Auxi';

const withErrorHandler = (WrappedComponent, axios) => {
    return class extends Component {
        state = {
            error: null
        }
        componentWillMount(){
            this.reqInterceptor = axios.interceptors.request.use(config =>{
                this.setState({error: null});
                return config;
            });
            this.resInterceptor = axios.interceptors.response.use(response => response, 
                error => {
                this.setState({error: error})
              });
        }
        componentWillUnmount(){
            axios.interceptors.request.eject(this.reqInterceptor);
            axios.interceptors.response.eject(this.resInterceptor);
        }
        handleDismissError = () => {
            this.setState({error: false});
        }
        render() {
            return (
                <Auxi>
                    <Modal show={this.state.error}
                    modalClicked={this.handleDismissError}>
                        {this.state.error ? this.state.error.message : null}
                    </Modal>
                    <WrappedComponent {...this.props}/>
                </Auxi>
            )
        }
    }
}

export default withErrorHandler