import React, { Component } from 'react';
import { Route, Switch } from 'react-router';

import Layout from './hoc/Layout/Layout';
import Auxi from './hoc/Auxs/Auxi';
import BurgerBuilder from './containers/BurgerBuilder/BurgerBuilder';
import Checkout from './containers/Checkout/Checkout';
import Orders from './containers/Orders/Orders';

class App extends Component {
  render() {
    return <Auxi>
      <Layout>
        <Switch>
          <Route path="/checkout" component={Checkout} />
          <Route path="/orders" component={Orders} />
          <Route path="/" exact component={BurgerBuilder} />
        </Switch>
      </Layout>
    </Auxi>

  }
}

export default App;
