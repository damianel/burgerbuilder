import React from 'react';

import NavigationItem from './NavigationItem/NavigationItem';
import classes from './NavigationItems.css'

const navigationItems = (props) => (
    <div className={classes.NavigationItems}>
        <NavigationItem exact link="/" name="BuildBuildier"/>
        <NavigationItem link="/orders" name="Orders"/>
    </div>
);

export default navigationItems;