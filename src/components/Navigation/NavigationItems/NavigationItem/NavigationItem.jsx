import React from 'react';
import { NavLink } from 'react-router-dom'

import classes from './NavigationItem.css';

const navigationItem = (props) => (
    <div className={classes.NavigationItem}>
        <NavLink
            to={props.link}
            activeClassName={classes.active}
            exact={props.exact}
        >
            {props.name}
        </NavLink>
    </div>
);

export default navigationItem;