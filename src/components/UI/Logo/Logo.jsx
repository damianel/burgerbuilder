import React from 'react';

import classes from './Logo.css';
import logoImg from '../../../assets/img/burger-logo.png';

const logo = (props) => (<img className={[classes.Logo, props.extraClass].join(' ')} alt="Logo" src={logoImg}/>)

export default logo;