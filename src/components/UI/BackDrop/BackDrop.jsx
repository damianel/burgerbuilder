import React from 'react';

import classes from './BackDrop.css';

const backDrop = (props) => (
    <div 
    onClick={props.clicked}
    className={[classes.BackDrop, props.show ? classes.show : ''].join(' ')}></div>
)

export default backDrop;