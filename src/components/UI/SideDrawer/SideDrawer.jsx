import React from 'react';


import classes from './SideDrawer.css';
import NavigationItems from '../../Navigation/NavigationItems/NavigationItems';
import Logo from '../Logo/Logo';
import BackDrop from '../BackDrop/BackDrop';
import Auxi from '../../../hoc/Auxs/Auxi';

const sideDrawer = (props) => (
    <Auxi>
        <BackDrop show={props.show} clicked={props.backDropClick}/>
        <div className={classes.SideDrawer}
            style={{ transform: props.show ? 'translateX(0)' : 'translateX(-120%)' }}>
            <Logo />
            <NavigationItems />
        </div>
    </Auxi>
);

export default sideDrawer;