import React from 'react';
import classes from './SideDrawerToggle.css';

const sideDrawerToggle = (props) => (
    <span onClick={props.clicked}
    className={classes.SideDrawerToggle}>{props.name}</span>
);

export default sideDrawerToggle;