import React, { Component } from 'react';

import BackDrop from '../BackDrop/BackDrop';
import Auxi from '../../../hoc/Auxs/Auxi'
import clasess from './Modal.css';

class Modal extends Component {

    // LIfeCycle componentWillMount(){     console.log(`[${this.constructor.name}]
    // componentWillMount`); } componentDidMount(){
    // console.log(`[${this.constructor.name}] componentDidMount`); console.log('
    // '); } componentWillReceiveProps(){ console.log(`[${this.constructor.name}]
    // componentWillReceiveProps`); }
    shouldComponentUpdate(nextProps) {
        // console.log(`[${this.constructor.name}] shouldComponentUpdate`);
        return this.props.show !== nextProps.show || this.props.children !== nextProps.children;
    }
    // componentWillUpdate(){     console.log(`[${this.constructor.name}]
    // componentWillUpdate`); } componentDidUpdate(){
    // console.log(`[${this.constructor.name}] componentDidUpdate`); console.log('
    // '); }

    render() {
        // console.log(`[${this.constructor.name}] render`);
        return (
            <Auxi>
                <BackDrop show={this.props.show} clicked={this.props.modalClicked} />
                <div
                    className={clasess.Modal}
                    style={{
                        transform: this.props.show
                            ? 'translateY(0)'
                            : 'translateY(-100vh)'
                    }}>
                    {this.props.children
                    }
                </div>
            </Auxi>
        )
    }

}

export default Modal;