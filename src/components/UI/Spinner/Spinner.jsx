import React from 'react';

import classes from "./Spinner.css";
const spinner = () => {
    return (
        <div className={classes.SpinnerGrid}>
        <div className={[classes.SpinnerCube, classes.SpinnerCube1].join(' ')}></div>
        <div className={[classes.SpinnerCube, classes.SpinnerCube2].join(' ')}></div>
        <div className={[classes.SpinnerCube, classes.SpinnerCube3].join(' ')}></div>
        <div className={[classes.SpinnerCube, classes.SpinnerCube4].join(' ')}></div>
        <div className={[classes.SpinnerCube, classes.SpinnerCube5].join(' ')}></div>
        <div className={[classes.SpinnerCube, classes.SpinnerCube6].join(' ')}></div>
        <div className={[classes.SpinnerCube, classes.SpinnerCube7].join(' ')}></div>
        <div className={[classes.SpinnerCube, classes.SpinnerCube8].join(' ')}></div>
        <div className={[classes.SpinnerCube, classes.SpinnerCube9].join(' ')}></div>
      </div>
    );
};

export default spinner;