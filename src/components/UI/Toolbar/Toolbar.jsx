import React from 'react';

import classes from './Toolbar.css';
import NavigationItems from '../../Navigation/NavigationItems/NavigationItems';
import SideDrawerToggle from '../SideDrawer/SideDrawerToggle/SideDrawerToggle';
import Logo from '../Logo/Logo';

const toolbar = (props) => (
    <header className={classes.Toolbar}>
        <SideDrawerToggle clicked={props.toggleSidedrawer} name="Menu"/>
        <Logo extraClass={classes.DesktopOnly}/>
        <nav className={classes.DesktopOnly}>
            <NavigationItems />
        </nav>
    </header>
);

export default toolbar;