import React from 'react';
import classes from './Input.css';

const input = (props) => {
    let inputElement = null;
    let inputStyleClass = (!props.valid && props.touched) ? classes.validationError : null;
    switch (props.elementType) {
        case 'input':
            inputElement = <input 
            onChange={props.changed} 
            className={inputStyleClass}
            {...props.config} />
            break;
        case 'textarea':
            inputElement = <textarea onChange={props.changed} {...props.config} />
            break;
        case 'select':
            inputElement = <select onChange={props.changed}>
                {props.config.options.map(option =>
                    (<option key={option.value} value={option.value}>{option.display}</option>)
                )}
            </select>
            break;
        default:
            inputElement = <input {...props.config} />
            break;
    }
    let labelElement = null;
    if (props.label) labelElement = <label>{props.label}</label>;

    return (
        <div className={classes.Input}>
            {labelElement}
            {inputElement}
        </div>
    )
}

export default input;