import React, {Component} from 'react';


import Button from '../../UI/Button/Button';
import classes from './OrderSummary.css';
import Auxi from '../../../hoc/Auxs/Auxi';


class OrderSummary extends Component {

    render(){
        
        const ingredientSummary = Object
        .keys(this.props.ingredients)
        .map(igKey => (
            <li key={igKey}>{igKey}: {this.props.ingredients[igKey]}</li>
        ))
        return (
            <Auxi>
                <h4>Twoje zamówienie:</h4>
                <ul className={classes.OrderList}>
                    {ingredientSummary}
                </ul>
                <h4>Wartość zamówienia: {this.props.totalPrice.toFixed(2)} PLN</h4>
                <h5>Kontynuować realizację zamówienia</h5>
                <Button type="Negative"
                 textValue="Anuluj"
                 clicked={this.props.clickedHandlers.discardClick}/>
                <Button type="Positive" 
                textValue="Kontynuuj"
                clicked={this.props.clickedHandlers.continueClick}/>
            </Auxi>
        )
    }

}

export default OrderSummary;