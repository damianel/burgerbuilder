import React from 'react';

import classes from './BurgerControl.css';

const burgerControl = (props) => (
    <div className={classes.Control}>
        <span>{props.ingredient.label}</span>
        <button 
            className={classes.Less}
            onClick={() => (props.onRemoveIngredient(props.ingredient.type))}
            disabled={props.disabled}
            >Less</button>
        <button 
            className={classes.More}
            onClick={() => (props.onAddIngredient(props.ingredient.type))}>More</button>
    </div>
);

export default burgerControl;