import React from 'react';

import BurgerControl from './BurgerControl/BurgerControl';
import classes from './BurgerControls.css';
const controls = [
    {
        label: 'Meat',
        type: 'meat'
    }, {
        label: 'Cheese',
        type: 'cheese'
    }, {
        label: 'Salad',
        type: 'salad'
    }, {
        label: 'Bacon',
        type: 'bacon'
    }
]
const burgerControls = (props) => (
    <div className={classes.ControlPanel}>
        <p>Wartość zamówienia: {props
                .totalPrice
                .toFixed(2)}</p>
        {controls.map(ctrl => (<BurgerControl
            key={ctrl.type}
            ingredient={ctrl}
            onRemoveIngredient={props.removedIngredient}
            onAddIngredient={props.addedIngredient}
            disabled={props.disabled[ctrl.type]}/>))}
        <button
            className={classes.OrderButton}
            disabled={!props.purchaseable}
            onClick={props.clicked}>ORDER NOW</button>
    </div>
);

export default burgerControls;