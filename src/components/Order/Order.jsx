import React from 'react';
import classes from './Order.css';

const order = (props) => {
    let ingredients = [];
    for(let key in props.ingredients){
        ingredients.push(`(${key} ${props.ingredients[key]})`)
    }
    ingredients = ingredients.join(', ');
    return (
        <div className={classes.Order}>
            <p>Składniki: {ingredients}</p>
            <p>Cena: <b>PLN</b> {props.price}</p>
        </div>
    )
}

export default order;