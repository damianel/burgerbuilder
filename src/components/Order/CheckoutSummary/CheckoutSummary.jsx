import React from 'react';
import classes from './CheckoutSummary.css'

import Burger from '../../Burger/Burger';
import Button from '../../UI/Button/Button'

const checkoutSummary = (props) => {
    return (
        <div className={classes.CheckoutSummary}>
            <Burger ingredients={props.ingredients} />
            <Button
                type="Positive"
                clicked={props.checkoutContinued}
                textValue="Kosntynuuj"
            />
            <Button
                type="Negative"
                clicked={props.checkoutCanceled}
                textValue="Anuluj"
            />
        </div>
    )
}

export default checkoutSummary;