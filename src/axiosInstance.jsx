import axios from 'axios';

const axiosBurgerInstance = axios.create();
axiosBurgerInstance.defaults.baseURL = 'https://burgertutorial.firebaseio.com/';

export default axiosBurgerInstance;